$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#toggleIcon").toggleClass("fa fa-angle-double-right fa")
    $("#wrapper").toggleClass("toggled");
    if ($("#wrapper").hasClass("toggled")) {
        $("#menu-toggle").addClass("btn-outline-primary");
        $("#menu-toggle").removeClass("btn-primary");
    } else {
        $("#menu-toggle").removeClass("btn-outline-primary");
        $("#menu-toggle").addClass("btn-primary");
    }
});