
var csrfToken = $("#csrfToken").val();
var hostname = window.location.origin;
var maxLen = 140;

// loader
$(window).on('load',function() {
    $("div.loader-wrapper").hide();
    $("#content").css("display","block");
});

setTimeout(function () {
    $(".flashMessage").hide('fade');
},6000);

// // Adding post function
// $("#addPostBtn").click(function(e) {
//     e.preventDefault();
//     var textarea = $("#addPostTextArea");
//     var post = textarea.val().trim();
//     if (post.length !== 0 ) {
//         var data = `post=${post}`;
//         var xhr = new XMLHttpRequest;
//         xhr.onload = () => {
//             let response = null;
//             try {
//                 response = JSON.parse(xhr.responseText);
//                 if (response.status == "posted") {
//                     location.reload();
//                 } else if (response.status == 'null') {
//                     redAlert("You cannot post a blank text");
//                 }
                
//             } catch (e) {
//                 console.log('Could not parse JSON!');
//                 redAlert("Error in adding your post.");
//             }
//         }
//         xhr.open("POST" , hostname + "/microblog2/posts/add", true);
//         xhr.setRequestHeader("X-CSRF-Token", csrfToken);
//         xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
//         xhr.send(data);
//     } else {
//         redAlert("You cannot post a blank text");
//     }
// })

// Editing post function
$(".edit-post.actions").click(function(e) {
    e.preventDefault();
    var postId = e.target.id;
    var editDiv = $("div#edit-post" + postId);
    var postDiv = $("p#post-" + postId);
    var postText = postDiv.text();
    var textArea = $("#textarea-" + postId);
    var postActions = $("div.post-actions");
    var cancelBtn = $("#cancelBtn-" + postId);
    var saveChangesBtn = $("#button-" + postId);
    editDiv.css("display","block");
    postDiv.css("display","none");
    cancelBtn.css("display","block");
    postActions.css("display","none");
    textArea.focus();
    textArea.text(postText);
    charsLeft();

    cancelBtn.click(function(e) {
        postActions.css("display","block");
        postDiv.css("display","block");
        editDiv.css("display","none");
        cancelBtn.css("display","none");
        textArea.val(postText);
    })
    
    textArea.keyup(charsLeft);
    function charsLeft() {
        var remainingLen = maxLen - (textArea.val()).length;
        var count = $("#charCounter-" + postId);
        count.text(remainingLen + " characters left.");
    }
    
    saveChangesBtn.click(function(e) {
        var textAreaText = textArea.val().trim();        
        if (textAreaText.length !== 0 || ($("#repost-"+postId).text()).length !== 0) {
            var xhr = new XMLHttpRequest;
            xhr.onload = () => {
                let response = null;
                try {
                    response = JSON.parse(xhr.responseText);
                    if (response.status == "edited") {
                        postDiv.text(textAreaText);
                        postActions.css("display","block");
                        postDiv.css("display","block");
                        editDiv.css("display","none");
                        cancelBtn.css("display","none");
                        showAlert("Your post has been edited successfully");
                    } else if (response.status == "null") {
                        redAlert("You cannot post a blank text");
                    }
                } catch (e) {
                    console.error('Could not parse JSON!');
                    redAlert('Error in editing your post');
                }
            }
            var data = `post=${textAreaText}`;
            xhr.open("POST", hostname + "/posts/edit/"+postId, true);
            xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
            xhr.send(data);
        } else {
            redAlert("You cannot post a blank text");
        }
    });

})

// Deleting post function
$(".delete-post.actions").click(function(e) {
    if (confirm("Are you sure you want to delete this post?")) {
        e.preventDefault();
        var postId = e.target.id;
        var repostId = e.target.name;
        var xhr = new XMLHttpRequest;
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);            
                location.reload();            
            } catch (event) {
                console.error('Could not parse JSON!');
                redAlert("Error in deleting your post");
            }
        }
        xhr.open("GET", hostname + "/posts/delete/"+postId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send();
    } else {
        return false;
    }


});

// like and Unlike function
$(".like").click(function(e) {
    e.preventDefault();
    var postId = e.target.id;
    var likeButton = $(".like-" + postId);
    const xhr = new XMLHttpRequest;
    var noOfLikes = $(".likeCounter-" + postId);
    var currentNoOfLikes = Number(noOfLikes.text());
    if (likeButton.hasClass("btn-outline-primary")) { //like
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'liked') {
                    likeButton.addClass("btn-primary");
                    likeButton.addClass("unlike");
                    likeButton.removeClass("btn-outline-primary");
                    noOfLikes.text(currentNoOfLikes + 1);
                }
            } catch (e) {
                console.error('Could not parse JSON!');
            }
        };
        xhr.open("GET", hostname + "/likes/add/"+postId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send();
    } else if (likeButton.hasClass("unlike")) { //unlike
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'deleted') {
                    likeButton.addClass("btn-outline-primary");
                    likeButton.removeClass("btn-primary");
                    likeButton.removeClass("unlike");
                    noOfLikes.text(currentNoOfLikes - 1);
                }
            } catch (e) {
                console.error('Could not parse JSON!');
            }
        };
        xhr.open("GET", hostname + "/likes/delete/"+postId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send(); 
    }



});

//Edit comment function
$(".edit-comment").click(function(e) {
    e.preventDefault();
    var commentId = e.target.id;
    var comment = $("p.card-text" + commentId);
    var commentText = comment.text();
    var textArea = $("#textarea-" + commentId);
    var commentActions = $(".commentActions");
    var editCommentDiv = $("div.edit-comment" + commentId);
    var cancelBtn = $("#cancelBtn-" + commentId);
    var saveChangesBtn = $("#button-" + commentId);
    textArea.text(commentText);
    commentActions.css("display","none");
    comment.css("display","none");
    editCommentDiv.css("display","block");
    cancelBtn.css("display","block");
    textArea.focus();
    charsLeft();
    
    cancelBtn.click(function(e) {
        commentActions.css("display","block");
        comment.css("display","block");
        editCommentDiv.css("display","none");
        cancelBtn.css("display","none");
        textArea.val(commentText);
    })

    textArea.keyup(charsLeft);
    function charsLeft() {
        var remainingLen = maxLen - (textArea.val()).length;
        var count = $("#charCounter-" + commentId);
        count.text(remainingLen + " characters left.");
    }

    saveChangesBtn.click(function(e) {

        var textAreaText = textArea.val().trim();
        if (textAreaText.length !== 0) {
            var xhr = new XMLHttpRequest;
            xhr.onload = () => {
                let response = null;
                try {
                    response = JSON.parse(xhr.responseText);
                    if (response.status == "edited") {
                        comment.text(textAreaText);
                        commentActions.css("display","block");
                        comment.css("display","block");
                        editCommentDiv.css("display","none");
                        cancelBtn.css("display","none");
                        showAlert("Your comment has been edited successfully");
                    } else if (response.status == "null") {
                        redAlert('You cannot comment a blank text');
                    }
                } catch (e) {
                    console.error('Could not parse JSON!');
                    redAlert("Error in edting your comment");                    
                }
            }
            var data = `comment=${textAreaText}`;
            xhr.open("POST",  hostname + "/comments/edit/"+commentId, true);
            xhr.setRequestHeader("X-CSRF-Token", csrfToken);
            xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
            xhr.send(data);
        }
    });
});

//Delete comment function
$(".delete-comment").click(function(e) {
    e.preventDefault();
    if (confirm("Are you sure you want to delete this comment?")) {
        var noOfComments = Number($("#noOfComments").text());
        var commentId = e.target.id;
        var xhr = new XMLHttpRequest;
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == "deleted") {
                    $("#commentDiv-" + commentId).css("display" ,"none");
                    $("#noOfComments").text(noOfComments - 1);
                    showAlert("Your comment has been deleted successfully");
                } 
            } catch (event) {
                console.error('Could not parse JSON!');
                redAlert('Error in deleting your comment');
            }
        }
        xhr.open("GET", hostname + "/comments/delete/"+commentId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send();
    }
});

//Retweet post function
$(".repost-save").click(function(e) {
    e.preventDefault();
    var postId = e.target.id;
    var post = $("textarea#repost-" + postId).val().trim();    
    var data = `post=${post}`;
    var xhr = new XMLHttpRequest;
    xhr.onload = () => {
        response = null;
        try {
            response = JSON.parse(xhr.responseText);
            location.reload();
        } catch (e) {
            console.log('Could not parse JSON!');
            redAlert('Error in retweeting a post');
        }
    }    
    xhr.open("POST" , hostname + "/reposts/add/" + postId, true);
    xhr.setRequestHeader("X-CSRF-Token", csrfToken);
    xhr.setRequestHeader("Content-type" , "application/x-www-form-urlencoded");
    xhr.send(data);    
})

// Showing profile picture function
var uploadedImage = $("#image-btn");
var currentImage = $("#current-image");
uploadedImage.on('change', function(e) {
    const file = this.files[0];

    if (file) {
        const reader = new FileReader();
        reader.addEventListener("load",function() {
            currentImage.attr("src" ,this.result);
        })
        reader.readAsDataURL(file);
    }
})

var notFollowingClass = "btn btn-outline-primary btn-sm float-right follow-button";
var followingClass = "btn btn-primary btn-sm float-right follow-button followed";
//Following Unfollowing function
$(".follow-button").click(function(e) {
    e.preventDefault();
    var followingId = e.target.id;
    const xhr = new XMLHttpRequest;
    //unfollow
    if (e.target.innerHTML == "Unfollow") {
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'unfollowed') {
                    e.target.className = notFollowingClass
                    e.target.innerHTML = "Follow";                    
                }
            } catch (e) {
                console.error('Could not parse JSON!');
                redAlert('Error in unfollowing a user');
            }
        };
        xhr.open("GET", hostname + "/followers/delete/"+followingId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send();
    } else if (e.target.innerHTML == "Follow") { //follow
        xhr.onload = () => {
            let response = null;
            try {
                response = JSON.parse(xhr.responseText);
                if (response.status == 'followed') {
                    e.target.className = followingClass;
                    e.target.innerHTML = "Following";                    
                }
            } catch (e) {
                console.error('Could not parse JSON!');
                redAlert('Error in following a user');
                console.error(xhr.responseText);
            }
        };
        xhr.open("GET", hostname + "/followers/add/"+followingId, true);
        xhr.setRequestHeader("X-CSRF-Token", csrfToken);
        xhr.send();
    }

})

$(".follow-button").hover(function(e){
    if (e.target.className == followingClass) {
        e.target.className = "btn btn-danger btn-sm float-right follow-button followed"
        e.target.innerHTML = "Unfollow";  
    }
}, function(e){
    if (e.target.className == "btn btn-danger btn-sm float-right follow-button followed") {
        e.target.className = followingClass
        e.target.innerHTML = "Following";
    }
})

$("#showPostsBtn").click(function(e) {
    e.preventDefault();
    $("#showPosts").css("display","block");
    $("#showFollowings").css("display","none");
    $("#showFollowers").css("display","none");
})

$("#showFollowingsBtn").click(function(e) {
    e.preventDefault();
    $("#showPosts").css("display","none");
    $("#showFollowings").css("display","block");
    $("#showFollowers").css("display","none");
})

$("#showFollowersBtn").click(function(e) {
    e.preventDefault();
    $("#showPosts").css("display","none");
    $("#showFollowings").css("display","none");
    $("#showFollowers").css("display","block");
})


if ($("textarea.mt-5").length) {
    document.querySelector("textarea.mt-5").addEventListener("keyup",function() {
        var remainingLen = maxLen - (document.querySelector("textarea.mt-5").value).length;
        var count = document.getElementById('charCounter');
        count.innerHTML =  remainingLen + " characters left.";
    });
}

function showAlert(message) {
    if ($("#custom-alert").length) {
        $("#custom-alert").remove();
    }
    clearTimeout(settingTime);
    $('<strong id="custom-alert">' + message +'</strong>').appendTo("#myAlert");
    $("#myAlert").attr("class","alert alert-success collapse");
    $("#myAlert").show('fade');
    var settingTime = setTimeout(function () {
        $("#myAlert").hide('fade');
    },6000);
    $("#alertClose").click(function(){
        $("#myAlert").hide('fade');
    })
}

function redAlert(message) {
    if ($("#custom-alert").length) {
        $("#custom-alert").remove();
    }
    clearTimeout(settingTime);
    $('<strong id="custom-alert">' + message +'</strong>').appendTo("#myAlert");
    $("#myAlert").attr("class","alert alert-danger collapse");
    $("#myAlert").show('fade');
    var settingTime = setTimeout(function () {
        $("#myAlert").hide('fade');
    },6000);
    $("#alertClose").click(function() {
        $("#myAlert").hide('fade');
    })
}