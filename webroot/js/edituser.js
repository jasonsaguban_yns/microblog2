
$("#basic-info-btn").remove();
const oUsername = $("#username").val();
const oEmail = $("#email").val();
const oFirstName = $("#first-name").val();
const oLastName = $("#last-name").val();
const oQuote = $("#quote").val();

window.setInterval(checkBasicChanges, 300);
window.setInterval(passwordval, 300);

function checkBasicChanges() {
   breakme: { 
       if ($("#username").val() != oUsername || $("#email").val() != oEmail || $("#first-name").val() != oFirstName || $("#last-name").val() != oLastName || $("#quote").val() != oQuote) {                        
            if ($("#basic-info-btn").length == 1) {                
                break breakme;
            }
            $('<button class="btn btn-outline-primary btn-sm float-right mb-3 mt-1" id="basic-info-btn" type="submit" style="display: block;">Save Changes</button>').appendTo("#basicUserInfo");            
        } else {
            $("#basic-info-btn").remove();
        }
    }
}

$("#image-btn").change(function(e) {
    if ($("#image-save-btn").length == 0) { 
        $('<button class="btn btn-outline-primary btn-sm float-right my-2" id="image-save-btn" type="submit">Save Changes</button>').appendTo("#userProfilePicture");
    }        
})

function passwordval() {
    breakthis: {
        if ($("#old-password").val().length >= 1 && $("#password1").val().length >=1 && $("#password2").val().length >= 1) {
            console.log('may laman')
            if ($("#password-btn").length) {                
                break breakthis;
            }
            $('<button class="btn btn-outline-primary btn-sm float-right mb-3" id="password-btn" type="submit">Save Changes</button>').appendTo("#userPasswordEdit");
        } else {
            $("#password-btn").remove();
        }
    }
}

