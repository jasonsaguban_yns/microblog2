<?php

use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;


Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    $routes->setExtensions(['json','xml']);    
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    $routes->applyMiddleware('csrf');

    $routes->connect('/', ['controller' => 'Posts', 'action' => 'index']);
    $routes->connect('/post/*', ['controller' => 'Posts', 'action' => 'view']);
    $routes->connect('/user/*', ['controller' => 'Users', 'action' => 'view']);
    $routes->connect('/me/edit', ['controller' => 'Users', 'action' => 'edit']);
    $routes->connect('/log-in',['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/log-out',['controller' => 'Users', 'action' => 'logout']);
    $routes->connect('/register',['controller' => 'Users', 'action' => 'register']);
    $routes->connect('/verify/*',['controller' => 'Users', 'action' => 'verify']);
    $routes->connect('/searchuser/*',['controller' => 'Users', 'action' => 'searchuser']);
    $routes->connect('/searchpost/*',['controller' => 'Users', 'action' => 'searchpost']);    
    $routes->connect('/change-password/*',['controller' => 'Users', 'action' => 'changepassword']);
    $routes->connect('/forgot-password',['controller' => 'Users', 'action' => 'forgotpassword']);

    $routes->fallbacks(DashedRoute::class);
});
