<div class="text-center  mx-auto col-3 mt-2">
<?= $this->Html->image('microblog-logo.png') ?>
</div>
<div class="card border-primary my-4 mx-auto " style="max-width: 25rem;">
<div class="card-header">
    <h4 class="text-center text-primary"><strong>REGISTRATION</strong></h4>
</div>
<div class="card-body">
<?php echo $this->Form->create($user, ['noValidate' => true]); ?>
    <fieldset>
        <?php echo $this->Form->control(
            'username',
            array('class' => 'form-control mb-2', 'placeholder' => 'Username','label' => 'Username' , 'required' => false )
        );
        echo $this->Form->control(
            'password',
            array('class' => 'form-control', 'placeholder' => 'Password','label' => 'Password', 'required' => false )
        );

        echo $this->Form->control(
            'email',
            array('class' => 'form-control mb-2', 'placeholder' => 'Email','label' => 'Email', 'required' => false )
        );

        echo $this->Form->control(
            'first_name',
            array('class' => 'form-control mb-2', 'placeholder' => 'First Name','label' => 'First Name', 'required' => false )
        );

        echo $this->Form->control(
            'last_name',
            array('class' => 'form-control mb-2', 'placeholder' => 'Last Name','label' => 'Last Name', 'required' => false )
        );

        ?>
    </fieldset>
    <?= $this->Form->button('Register', array('class' => 'btn btn-primary my-2 float-right ml-2')) ?>
    <a href="<?= $this->Url->build(['action' => 'login']) ?>" class="btn btn-outline-success my-2 float-right">Login</a>    
<?php echo $this->Form->end() ?>
</div>
</div>
