<div class="text-center mt-5 mx-auto col-3">
    <?= $this->Html->image('microblog-logo.png') ?>
</div>
<div class="card border-primary mt-4 mx-auto " style="max-width: 20rem;">
    <div class="card-header">
        <h2 class="text-center text-primary"><strong>LOGIN</strong></h2>
    </div>
<div class="card-body">
<?php echo $this->Flash->render('auth'); ?>
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <?php
            echo $this->Form->input(
                'username',
                array('class' => 'form-control mb-0', 'placeholder' => 'Username/Email','label' => false )
            );

            echo $this->Form->input(
                'password',
                array('class' => 'form-control', 'placeholder' => 'Password','label' => false )
            );
            ?>            
    </fieldset>    
    <div class="text-center mb-2">
    <?= $this->Form->control('remember_me', ['type' => 'checkbox','label'=>'Remember me']) ?>
    <a href="<?= $this->Url->build(['action' => 'forgotpassword']) ?>" class='d-block mb-1'>Forgot Password?</a>
    <a href="<?= $this->Url->build(['action' => 'register']) ?>" class="btn btn-outline-success">Register</a>
    <?= $this->Form->button('Login', array('class' => 'btn btn-primary')) ?>
    </div>
<?php echo $this->Form->end(); ?>
</div>
</div>
