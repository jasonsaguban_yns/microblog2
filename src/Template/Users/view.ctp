<?= $this->Html->css('custom.css') ?>
<div class="card border-primary mt-4 mb-2 mx-auto" style="max-width: 35rem;">
    <div class="card-header">
        <?php if ($this->Session->read('Auth.User.id') !== $user->id) : ?>
            <?php
                $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                $followBtnVal = "Follow";
            foreach ($followingList as $followingId) {
                if ($followingId->following_user_id == $user->id) {
                    $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                    $followBtnVal = "Following";
                    break;
                }
            }
            ?>
            <button class="<?= $followBtnClass ?>"" style="display:inline" id=<?= $user->id ?>><?= $followBtnVal ?></button>
        <?php endif ?>
        <?= $this->Html->image(($user->image_location) ? h(($user->image_location)) : 'microblogicon.png', ['class' => 'float-left mr-1 mt-2 profile-picture']) ?>
        <strong style="font-size: 30px;display: inline-block;"><?= ucfirst(h($user->first_name))." ".ucfirst(h($user->last_name)) ?></strong><br>
        <h6 class="card-subtitle text-muted mt-1">@<?= h($user->username) ?></h6>
        <p class="card-text mt-4"><small class="text-muted">Joined MicroBlog since: <br> <?= date('F j, Y', strtotime($user->created)) ?></small></p>
        <span class="float-right "><a id='showFollowingsBtn' href="">Following: <strong><?= count($followings) ?></strong></a></span>
        <span class="float-right mr-3"><a id='showFollowersBtn' href="">Followers: <strong><?= count($followers) ?></strong></a></span>
        <span class="float-right mr-3"><a id='showPostsBtn' href="">Posts: <strong><?= count($posts) ?></strong></a></span>
    </div>
</div>

<div class="mx-auto" style="max-width: 35rem;">
    <blockquote class="blockquote text-center" id='user-quote'>
        <?php
        if ($user->quote) {
            $quote = '"'.$user->quote.'"';
        } else {
            $quote = 'No quote yet';
        }
        ?>
        <p><i><?= $quote ?></i></p>
    </blockquote>
</div>





<hr class="bg-primary mx-auto" style="max-width: 35rem; border-width: 5px">

<!-- Adding post section -->
<?php if ($user->id == $this->Session->read('Auth.User.id')) : ?>
    <?= $this->element('addpost') ?>
<?php endif ?>

<!-- posts view section -->
<div id="showPosts">
    <?php if (count($posts)) : ?>
        <?= $this->element('posts') ?>
        <?= $this->element('pagination') ?>
    <?php else : ?>
        <?php if ($user->id == $this->Session->read('Auth.User.id')) : ?>
            <p class="lead text-center text-primary mx-auto" style="max-width: 35rem;">
                You don't have any post yet. Post now or follow other user's to see their post.
            </p>
        <?php else : ?>
            <p class="lead text-center text-primary mx-auto" style="max-width: 35rem;">
                <?= ucfirst(h($user->first_name))." doesn't have any post yet." ?>
            </p>
        <?php endif ?>
    <?php endif ?>
</div>

<!-- followers view section -->
<div id="showFollowers" class="card border-primary mx-auto mt-3"  style="max-width: 35rem;display:none;">
<?php if (count($followers)) : ?>
    <h4 class='my-3' align='center'><strong><?= ucfirst(h($user->first_name))." ".ucfirst(h($user->last_name))."'s Followers" ?></strong></h4>
    <?php foreach ($followers as $follower) : ?>
    <div class="card-header mt-2" style="border-width: 1px;">
        <a href=<?= $this->Url->build(['controller' => 'users','action' => 'view',$follower->User['username'] ]) ?>>
        <?= $this->Html->image(
            $follower->User['image_location'] ? h($follower->User['image_location']) : 'microblogicon.png',
            ['class' => 'float-left mr-1 picture-1']
        ) ?>
        <?= ucfirst(h($follower->User['first_name']))." ".ucfirst(h($follower->User['last_name'])) ?>
        <small class="card-subtitle text-muted" style="display:block">@<?= h($follower['User']['username']) ?></small>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $follower->User['id']) : ?>
            <?php
                $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                $followBtnVal = "Follow";
            foreach ($followingList as $followingId) {
                if ($followingId->following_user_id == $follower->User['id']) {
                    $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                    $followBtnVal = "Following";
                    break;
                }
            }
            ?>
            <button class="<?= $followBtnClass ?>"" style="position: relative; right: 5px;bottom: 30px;" id=<?= $follower->User['id'] ?>><?= $followBtnVal ?></button>
        <?php endif ?>
    </div>
    <?php endforeach ?>
<?php else : ?>
    <p class="lead text-center text-primary">
        <?= ucfirst(h($user->first_name))." doesn't have any followers yet." ?>
    </p>
<?php endif ?>
</div>



<!-- followings view section -->
<div id="showFollowings" class="card border-primary mx-auto mt-3"  style="max-width: 35rem;display:none;">
<?php if (count($followings)) : ?>
    <h4 class='my-3' align='center'><strong><?= ucfirst(h($user->first_name))." ".ucfirst(h($user->last_name))."'s Following List" ?></strong></h4>
    <?php foreach ($followings as $follows) : ?>
    <div class="card-header mt-2" style="border-width: 1px;">
        <a href=<?= $this->Url->build(['controller' => 'users','action' => 'view',$follows->User['username']])?>>
        <?= $this->Html->image(
            $follows->User['image_location'] ? h($follows->User['image_location']) : 'microblogicon.png',
            ['class' => 'float-left mr-1 picture-1']
        ) ?>
        <?= ucfirst(h($follows->User['first_name']))." ".ucfirst(h($follows->User['last_name'])) ?>
        <small class="card-subtitle text-muted" style="display:block">@<?= h($follows->User['username']) ?></small>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $follows->User['id']) : ?>
            <?php
                $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                $followBtnVal = "Follow";
            foreach ($followingList as $followingId) {
                if ($followingId->following_user_id == $follows->User['id']) {
                    $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                    $followBtnVal = "Following";
                    break;
                }
            }
            ?>
            <button class="<?= $followBtnClass ?>"" style="position: relative; right: 5px;bottom: 30px;" id=<?= $follows->User['id'] ?>><?= $followBtnVal ?></button>
        <?php endif ?>
        <small class="card-subtitle text-muted float-right" style="position: relative; right: 10px;bottom: 20px;">
            <?php
                $follow = '';
            foreach ($followerListId as $followerId) {
                if ($followerId->follower_user_id == $follows->User['id']) {
                    $follow = 'Follows you';
                    break;
                }
            }
            ?>
            <i><?= $follow ?></i>
        </small>

    </div>
    <?php endforeach ?>
<?php else : ?>
    <p class="lead text-center text-primary">
        <?= ucfirst(h($user->first_name))." doesn't follow anyone yet." ?>
    </p>
<?php endif ?>
</div>