<?= $this->Html->css('custom.css') ?>
<div class="card border-primary mx-auto"  style="max-width: 35rem;">
    <h4 class='my-3' align='center'>Search results for: <strong><?= h($searchVal) ?></strong></h4>
    <div class="btn-group" role="group" aria-label="Basic example">
        <a href=<?= $this->Url->build(['controller' => 'users','action'=>'searchuser','?'=>['val'=>$searchVal]]) ?> class="btn btn-outline-primary">
            Users<span class="badge"><?= $resultCount ?></span>
        </a>
        <a href=<?= $this->Url->build(['controller' => 'users','action'=>'searchpost','?'=>['val'=>$searchVal]]) ?> class="btn btn-primary">
            Posts<span class="badge"><?= $postsCount ?></span>
        </a>
    </div>
</div>

<?= $this->element('posts', ['searchVal' => $searchVal]) ?>
<?= $this->element('pagination') ?>