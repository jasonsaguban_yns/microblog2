<div class="text-center mt-5 mx-auto col-4">
<?= $this->Html->image('microblog-logo.png') ?>
</div>
<div class="card border-primary my-4 mx-auto " style="max-width: 25rem;">
    <div class="card-header">
        <h2 class="text-center text-primary"><strong>CHANGE PASSWORD</strong></h2>
    </div>
<div class="card-body">
<?= $this->Form->create($user, array('id' =>'forgotPassword','noValidate' => true)) ?>
    <?= $this->Form->input(
        'password1',
        ['class' => 'form-control', 'placeholder' => 'New Password','label' => 'New Password','type' => 'password', 'required' => false]
    )
    ?>
    <?= $this->Form->input(
        'password2',
        ['class' => 'form-control', 'placeholder' => 'Re-type New Password','label' => 'Re-type New Password','type' => 'password', 'required' => false]
    )
    ?>
    <div class="float-right my-2">
    <a href="<?= $this->Url->build(['action' => 'login']) ?>" class="btn btn-outline-success">Login</a>
    <?= $this->Form->button('Save Changes', array('class' => 'btn btn-primary')) ?>
    </div>
    
</div>
</div>
<?= $this->Form->end() ?>