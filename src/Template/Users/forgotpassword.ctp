<div class="text-center mt-5 mx-auto col-4">
    <?= $this->Html->image('microblog-logo.png') ?>
</div>
<div class="card border-primary my-0 mx-auto " style="max-width: 25rem;">
<div class="card-header">
    <h4 class="text-center text-primary"><strong>FORGOTTEN PASSWORD</strong></h4>
</div>
<div class="card-body">
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <?= $this->Form->input(
            'email',
            ['class' => 'form-control mb-1', 'placeholder' => 'Email','label' => 'Enter your email']
        )?>                        
    </fieldset>
    <div class="float-right my-2">
    <a href="<?= $this->Url->build(['action' => 'login']) ?>" class="btn btn-outline-success">Login</a>
    <?= $this->Form->button('Submit', array('class' => 'btn btn-primary')) ?>
    </div>
<?= $this->Form->end() ?>

</div>
</div>
