<?php
    $userImage = h($user->image_location);
    $firstName = ucfirst(h($user->first_name));
    $lastName = ucfirst(h($user->last_name));
    $username = h($user->username);
    $email = h($user->email);
?>
<div class="card border-primary my-4 mx-auto " style="max-width: 35rem;">
    <!-- User profile picture -->
    <div class="card-header">
        <?= $this->Html->image($userImage ? $userImage : 'microblogicon.png', ['id' => 'current-image','class' => 'frounded mx-auto d-block edit-picture']) ?>
        <?= $this->Form->create($user, array('type'=>'file','id' => 'userProfilePicture')); ?>
        <?= $this->Form->input(
            'image_location',
            array('class' => 'mt-2 mx-auto', 'label' => false,'type'=>'file','id' => 'image-btn' )
        ) ?>

        <?= $this->Form->end() ?>
    </div>

    <div class="card-body">
        <div class="accordion" id="accordionExample">
            <!-- Basic user informations -->
            <div class="card">
                <div class="card-header p-0" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-sm btn-outline-primary btn-block text-left p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Basic User Informations
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <?= $this->Form->create($user, array('id' =>'basicUserInfo','noValidate' => true)) ?>
                        <?php
                        echo $this->Form->control(
                            'username',
                            array('class' => 'form-control my-2', 'placeholder' => 'Username','label' => 'Username', 'required' => false )
                        );
                        echo $this->Form->control(
                            'email',
                            array('class' => 'form-control my-2', 'placeholder' => 'Email','label' => 'Email' , 'required' => false)
                        );
                        echo $this->Form->control(
                            'first_name',
                            array('class' => 'form-control my-2', 'placeholder' => 'First Name','label' => 'First Name', 'required' => false )
                        );
                        echo $this->Form->control(
                            'last_name',
                            array('class' => 'form-control my-2', 'placeholder' => 'Last Name','label' => 'Last Name', 'required' => false )
                        );
                        echo $this->Form->control(
                            'quote',
                            array('class' => 'form-control my-2', 'placeholder' => 'Your quote','label' => 'Quote','type' => 'textarea', 'required' => false )
                        );
                        ?>
                    </div>
                <?= $this->Form->end() ?>
                </div>
            </div>
            <!-- Password changer -->
            <div class="card">
                <div class="card-header p-0" id="headingTwo">
                    <h2 class="mb-0">
                        <button class="btn btn-sm btn-outline-primary btn-block text-left collapsed p-3 rounded-0" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        User Password Settings
                        </button>
                    </h2>
                </div>
                <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                    <?= $this->Form->create($user, array('id' =>'userPasswordEdit' ,'noValidate' => true)) ?>
                        <?= $this->Form->input(
                            'old_password',
                            array('class' => 'form-control', 'placeholder' => 'Current Password','label' => 'Current Password','type' => 'password', 'required' => false)
                        )?>
                        <?= $this->Form->input(
                            'password1',
                            array('class' => 'form-control', 'placeholder' => 'New Password','label' => 'New Password','type' => 'password', 'required' => false)
                        )?>
                        <?= $this->Form->input(
                            'password2',
                            array('class' => 'form-control', 'placeholder' => 'Re-type New Password','label' => 'Re-type New Password','type' => 'password', 'required' => false)
                        )?>
                    </div>
                    <?= $this->Form->end() ?>
                </div>
            </div>
        </div>
    </div>    
</div>

<?= $this->Html->script('edituser.js') ?>