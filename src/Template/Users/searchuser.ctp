<?= $this->Html->css('custom.css') ?>
<div class="card border-primary mx-auto"  style="max-width: 35rem;">
    <h4 class='my-3' align='center'>Search results for: <strong><?= h($searchVal) ?></strong></h4>
    <div class="btn-group" role="group" aria-label="Basic example">
        <a href=<?= $this->Url->build(['controller' => 'users','action'=>'searchuser','?'=>['val'=>$searchVal]]) ?> class="btn btn-primary">
            Users<span class="badge"><?= $resultsCount ?></span>
        </a>
        <a href=<?= $this->Url->build(['controller' => 'users','action'=>'searchpost','?'=>['val'=>$searchVal]]) ?> class="btn btn-outline-primary">
            Posts<span class="badge"><?= $postsCount ?></span></a>
    </div>
</div>

<?php foreach ($results as $result) : ?>
<div id="showSearchResults" class="card border-primary mx-auto my-1"  style="max-width: 35rem;">
    <div class="card-header pt-2 px-3 pb-0" style="border-width: 1px;" id= <?= "commentDiv-".$result->id?>>
        <a href=<?= $this->Url->build(['controller' => 'users','action' => 'view',$result->username]) ?>>
        <?= $this->Html->image(
            $result->image_location ? h($result->image_location) : 'microblogicon.png',
            ['class' => 'float-left mr-1 picture-2']
        ) ?>
        <?php
            $name =  ucfirst(h($result->first_name))." ".ucfirst(h($result->last_name));
            $highlight = ['format' => '<span class="highlight">\1</span>'];
            $name = $this->Text->highlight($name, $searchVal, $highlight);
        ?>
        <h5><?= $name ?></h5>
        <h6 class="card-subtitle text-muted">@<?= $this->Text->highlight(h($result->username), $searchVal, $highlight) ?></h6>
        </a>
        <?php if ($this->Session->read('Auth.User.id') !== $result->id) : ?>
            <?php
                $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                $followBtnVal = "Follow";
            foreach ($followingList as $followingId) {
                if ($followingId->following_user_id == $result->id) {
                    $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                    $followBtnVal = "Following";
                    break;
                }
            }
            ?>
            <button class="<?= $followBtnClass ?>"" style="position: relative; right: 5px;bottom: 30px;" id=<?= $result->id ?>><?= $followBtnVal ?></button>
        <?php endif ?>
        <small class="card-subtitle text-muted float-right" style="position: relative; right: 10px;bottom: 20px;">
            <?php
                $follow = '';
            foreach ($followerListId as $followerId) {
                if ($followerId->follower_user_id == $result->id) {
                    $follow = 'Follows you';
                    break;
                }
            }
            ?>
            <i><?= $follow ?></i>
        </small>
    </div>
</div>    
<?php endforeach ?>

<?= $this->element('pagination') ?>
