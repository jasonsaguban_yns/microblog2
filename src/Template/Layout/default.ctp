
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
    microBlog
    </title>
    <?= 
        $this->Html->meta(
        'microblogicon.ico',
        '/microblogicon.ico',
        ['type' => 'icon']
        );
    ?>
    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <?= $this->Html->css('custom.css') ?>
    <?= $this->Html->css('pictures.css') ?>
    <?= $this->Html->css('logo.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>

    <?= $this->Html->script('jquery.js') ?>        
    <?= $this->element('navbar') ?>
    <div class="loader-wrapper" >
        <span class="loader"><span class="loader-inner"></span></span>
    </div>
    <?= $this->Flash->render()?>
    <?php 
        if ($this->Session->read('Auth.User.id')) {
            $class = "container clearfix mt-5";
        } else {
            $class = "container clearfix";
        }
    ?>
    <div class="<?= $class ?>" style="display:none" id="content">
    <?= $this->Html->image('microblog-logo-colored.png',['id'=>'logo-bg']) ?>
        <?= $this->element('sidebar') ?>
        <?= $this->fetch('content') ?>
        <?= $this->element('alert') ?>
    </div>
    <footer>
    <!-- JavaScript Bundle with Popper.js -->    
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0-alpha1/js/bootstrap.min.js" integrity="sha384-oesi62hOLfzrys4LxRF63OJCXdXDipiYWBnvTl9Y9/TRlw5xlKIEHpNyvvDShgf/" crossorigin="anonymous"></script>    
    <?= $this->Html->script('script.js') ?>
    </footer>
</body>
</html>
