<?php use Cake\I18n\Time; ?>
<?= $this->Html->css('custom.css') ?>

<?php
    $postId = $post->id;
    $postUserId = $post->user_id;
    $likeClass = "like btn btn-outline-primary post-actions like-".$postId;
for ($i = 0; $i < count($post->likes); $i++) {
    if ($this->Session->read('Auth.User.id') == $post->likes[$i]['user_id']) {
        $likeClass = "like btn btn-primary post-actions unlike like-".$postId;
        break;
    }
}
?>
<div class="card border-primary my-4 mx-auto" id=<?= "card-post-".$postId ?> style="max-width: 35rem;">
    <!-- header part of post section-->
    <div class="card-header">        
        <!-- actions to the post -->
        <button class="btn btn-sm btn-danger float-right" style="display:none;" id=<?="cancelBtn-".$postId?>>Cancel</button>
        <div class="btn-group float-right post-actions">
            <?php if ($postUserId === $this->Session->read('Auth.User.id')) : ?>
            <!-- Delete Post -->
                <button class="btn btn-outline-danger delete-post actions" id=<?= $postId ?> data-toggle="tooltip" title="Delete post" data-original-title="Delete post">
                <svg width="1em" height="2em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $postId ?>>
                <path id=<?= $postId ?> d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                <path id=<?= $postId ?> fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                </svg>
                </button>

            <!-- Edit Post -->
                <button class="btn btn-outline-secondary edit-post actions" id=<?= $postId ?> data-toggle="tooltip" title="Edit post" data-original-title="Edit post">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square edit-svg" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $postId?>>
                <path id=<?= $postId ?> d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                <path id=<?= $postId ?> fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                </svg>
                </button>
            <?php endif ?>                            
        </div>
        <!-- post user informations -->
        <div>
            <a href=<?= $this->Url->build(['controller' => 'users' , 'action' => 'view' , $post->user['username']])?>>
            <?= $this->Html->image(($post->user['image_location']) ? h($post->user['image_location']) : 'microblogicon.png', ['class' => 'float-left mr-1 picture-2']) ?>
            <?= ucfirst(h($post->user['first_name']))." ".ucfirst(h($post->user['last_name'])) ?>
            <small class="card-subtitle text-muted " style="display:block">@<?= h($post->user['username']) ?></small>
            </a>
            <?php $postedDate = new Time($post['created']) ?>                       
            <p class="card-text" style="display:inline"><small class="text-muted">
            <?= $postedDate->timeAgoInWords(['format' => 'MMM , YYY','accuracy' => ['month' => 'month'],'end' => '1 year']) ?>
            </small></p>
            <?php if ($post->user_id != $this->Session->read('Auth.User.id')) : ?>
                <?php
                    $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                    $followBtnVal = "Follow";
                foreach ($followingList as $followingId) {
                    if ($followingId->following_user_id == $post->user_id) {
                        $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                        $followBtnVal = "Following";
                        break;
                    }
                }
                ?>
                <button class="<?= $followBtnClass ?>"" style="display:inline;position:relative;right:25px;" id=<?= $post->user_id ?>><?= $followBtnVal ?></button>
                <br>
            <?php endif ?>
        </div>
    </div>
    
    <!-- contents of the posts section -->
    <div class="card-body text-black pb-0">
        <p class= 'card-text mx-3' id=<?= 'post-'.$postId ?> ><?= h($post->post) ?></p>
        <?php if ($post->post_image) : ?>
        <div class="text-center my-2">
            <?= $this->Html->image($post->post_image,['class' => 'post-picture']) ?>
        </div>
        <?php endif ?>
        <?php if ($postUserId === $this->Session->read('Auth.User.id')) : ?>
        <!-- editting of post section -->
        <div style="display: none;" class = "mb-5" id = <?= "edit-post".$postId ?>>
            <textarea name="editPost" id=<?= "textarea-".$postId ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
            <input type="hidden" name="postId" value=<?= $postId ?>>
            <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$postId ?>>Save Changes</button>
            <small class="ml-2" id=<?= "charCounter-".$postId ?>></small>
        </div>
        <?php endif ?>

        <!-- card for original post -->
        <?php if (!empty($post->Post_2['id'])) : ?>
        <div class="card border-secondary">
            <div class='card-header'>
                <a href=<?= $this->Url->build(['controller' => 'users' , 'action' => 'view' , $post->User_2['username']]) ?>>
                <?= $this->Html->image(($post->User_2['image_location']) ? h($post->User_2['image_location']) : 'microblogicon.png', ['class' => 'float-left mr-1 picture-1']) ?>
                <small>
                <?= ucfirst(h($post->User_2['first_name']))." ".ucfirst(h($post->User_2['last_name'])) ?></small>
                <small class="card-subtitle text-muted">@<?= h($post->User_2['username']) ?></small><br>
                </a>
                <?php $postedDate = new Time($post->Post_2['created']) ?>                            
                <small class="text-muted" style="display:inline">
                <?= $postedDate->timeAgoInWords(['format' => 'MMM , YYY','accuracy' => ['month' => 'month'],'end' => '1 year']) ?>
                </small>
                <?php if ($post->User_2['id'] != $this->Session->read('Auth.User.id')) : ?>
                    <?php
                        $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                        $followBtnVal = "Follow";
                    foreach ($followingList as $followingId) {
                        if ($followingId->following_user_id == $post->User_2['id']) {
                            $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                            $followBtnVal = "Following";
                            break;
                        }
                    }
                    ?>
                    <button class="<?= $followBtnClass ?>"" style="display:inline" id=<?= $post->User_2['id'] ?>><?= $followBtnVal ?></button>
                    <br>
                <?php endif ?>

            </div>
            <div class="card-body text-secondary">
            <small>
                <a href=<?= $this->Url->build(['controller' => 'posts','action' => 'view',$post->Post_2['id']]) ?>  class="float-right"><?= (empty($post->Post_2['deleted'])) ?  'View Original Post' : '' ?>
                </a>
            </small>
            <p class= 'card-text mt-3 ml-2' id=<?= 'repost-'.$postId ?>>
            <?php if (empty($post->Post_2['deleted'])) : ?>
                <?=  h($post->Post_2['post']); ?>
                <?php if ($post->Post_2['post_image']) : ?>
                    <div class="text-center my-2">
                        <?= $this->Html->image($post->Post_2['post_image'],['class' => 'post-picture']) ?>
                    </div>
                <?php endif ?>
            <?php else : ?>
                <?= '<strong><i>THIS POST WAS DELETED BY THE USER</i></strong>'; ?>
            <?php endif ?>
            </p>
            </div>
        </div>
        <?php endif ?>

        <!-- like,comment,repost section -->
        <hr class="bg-primary" style="max-width: 35rem; border-width: 1px">
        <div class="float-right mb-2">
            <a class="<?= $likeClass ?>" id=<?= $postId?> data-toggle="tooltip" title="Like" data-original-title="Delete post">
            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-heart" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $postId?>>
            <path fill-rule="evenodd" id=<?= $postId?> d="M8 2.748l-.717-.737C5.6.281 2.514.878 1.4 3.053c-.523 1.023-.641 2.5.314 4.385.92 1.815 2.834 3.989 6.286 6.357 3.452-2.368 5.365-4.542 6.286-6.357.955-1.886.838-3.362.314-4.385C13.486.878 10.4.28 8.717 2.01L8 2.748zM8 15C-7.333 4.868 3.279-3.04 7.824 1.143c.06.055.119.112.176.171a3.12 3.12 0 0 1 .176-.17C12.72-3.042 23.333 4.867 8 15z"/>
            </svg>
            <span class="<?= 'likeCounter-'.$postId." badge bg-outline-secondary" ?>"  id=<?= $postId?>><?= count($post->likes) ?></span> 
            </a>

            <a id='add-comment-btn' class="comment btn btn-outline-primary post-actions" data-toggle="tooltip" title="Comment" data-original-title="Comment">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-card-text" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M14.5 3h-13a.5.5 0 0 0-.5.5v9a.5.5 0 0 0 .5.5h13a.5.5 0 0 0 .5-.5v-9a.5.5 0 0 0-.5-.5zm-13-1A1.5 1.5 0 0 0 0 3.5v9A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-13z"/>
                <path fill-rule="evenodd" d="M3 5.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zM3 8a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9A.5.5 0 0 1 3 8zm0 2.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5z"/>
                </svg>
                <span class="badge bg-outline-secondary" id=<?= 'commentCounter-'.$postId ?>><?= count($post->comments) ?></span>
            </a>

            <button class="repost btn btn-outline-primary post-actions" data-toggle="modal" data-target=<?="#repostModal-".$postId ?> data-toggle="tooltip" title="Retweet" data-original-title="Retweet">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-repeat" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $postId?>>
                <path  d="M11.534 7h3.932a.25.25 0 0 1 .192.41l-1.966 2.36a.25.25 0 0 1-.384 0l-1.966-2.36a.25.25 0 0 1 .192-.41zm-11 2h3.932a.25.25 0 0 0 .192-.41L2.692 6.23a.25.25 0 0 0-.384 0L.342 8.59A.25.25 0 0 0 .534 9z"/>
                <path  fill-rule="evenodd" d="M8 3c-1.552 0-2.94.707-3.857 1.818a.5.5 0 1 1-.771-.636A6.002 6.002 0 0 1 13.917 7H12.9A5.002 5.002 0 0 0 8 3zM3.1 9a5.002 5.002 0 0 0 8.757 2.182.5.5 0 1 1 .771.636A6.002 6.002 0 0 1 2.083 9H3.1z"/>
                </svg>
                <span class="badge bg-outline-secondary" id=<?= 'repostCounter-'.$postId ?>><?= count($post->reposts) ?></span>
            </button>
            <div class="modal fade" id=<?="repostModal-".$postId ?> tabindex="-1" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Repost</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form>
                        <div class="mb-3">
                            <label for="message-text" class="col-form-label">Add to the post:</label>
                            <textarea id=<?= "repost-".$postId ?> rows="5" maxLength ='140' class="col-12" placeholder="What's on your mind?"></textarea>
                        </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary repost-save" id= <?= $postId ?>>Repost</button>
                    </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
