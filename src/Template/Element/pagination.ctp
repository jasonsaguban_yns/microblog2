<div class="mx-auto" style="max-width: 35rem;">
    <div class="mb-4 float-right">
        <ul class="pagination">
            <?= $this->Paginator->first('First ') ?>            
            <?= $this->Paginator->numbers(array('modulus' => 4)) ?>            
            <?= $this->Paginator->last(' Last') ?>
        </ul>
    </div>
</div>