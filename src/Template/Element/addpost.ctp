<div id='divAddPost' class="mx-auto" style="max-width: 35rem;">
    <?= $this->Form->create(false, ['type'=>'file','url' => ['controller' => 'Posts', 'action' => 'add']]) ?>
    <fieldset>
        <?php
            echo $this->Form->button('Post', array(
                'type' => 'submit' ,
                'class' => 'btn btn-primary float-right mt-5'));
            echo $this->Form->textarea('post', array(
                'maxLength' => '140',
                'class' => 'mt-5 col-10',
                'rows' => '5',
                'cols' => '30',
                'required' => false,
                'id' => 'add-post',
                'placeholder' => "What's on your mind, ".ucfirst(h($this->Session->read('Auth.User.first_name')))."?",
                'value' => isset($_SESSION['current-post']) ? h($_SESSION['current-post']) : ''));
            ?>
        <small id="charCounter"></small>
        <?= $this->Form->input('post_image',['label' => false,'type'=>'file']) ?>
    </fieldset>
    <?= $this->Form->end() ?>   
</div>
<?php 
    if (isset($_SESSION['current-post'])) {
        unset($_SESSION['current-post']); 
    } 
?>