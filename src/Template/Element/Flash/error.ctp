<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div id="flashMessage" class="alert alert-danger flashMessage" style="position: fixed; bottom: 10px;right:10px;width:300px;">
<a id="alertClose" href="#" class="close" data-dismiss="alert">&times</a>
    <strong id ="alert-message"><?= $message ?></strong>
</div>