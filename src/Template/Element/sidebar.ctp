<?php if ($this->Session->read('Auth.User.id')) : ?>
    <?= $this->Html->css('sidebars.css') ?>
    <?php if (count($suggest)) : ?>
<div id="wrapper">
    <!-- Sidebar -->
    <div id="sidebar-wrapper">
        <p class="lead text-center mt-1">Users you can follow:</p>
        <button id="menu-toggle" class="btn btn-primary btn-sm">
            <i id="toggleIcon" class="fa fa-angle-double-left"></i>
        </button>   
        <?php foreach ($suggest as $user) : ?>
        <div class="card border-primary my-1">
            <div class="card-header" style="border-width: 1px;">
                <a href=<?= $this->Url->build(['controller' => 'users','action' => 'view',$user->username]) ?>>
                <?= $this->Html->image(
                    $user->image_location ? h($user->image_location) : 'microblogicon.png',
                    ['class' => 'float-left mr-1 picture-2']
                ) ?>
                <?= mb_strimwidth(ucfirst(h($user->first_name))." ".ucfirst(h($user->last_name)), 0, 17, "...") ?>
                <small class="card-subtitle text-muted" style="display:block">@<?= mb_strimwidth(h($user->username), 0, 15, "...") ?></small>
                </a>
                <button class="btn btn-outline-primary btn-sm float-right follow-button" style="position:absolute; padding: 0 8px" id=<?= $user->id ?>>Follow</button> 
                <small class="card-subtitle text-muted" style="position:absolute; padding: 0 8px;right:1px;bottom:15px;">
                <?php
                    $follow = '';
                foreach ($followerListId as $followerId) {
                    if ($followerId->follower_user_id == $user->id) {
                        $follow = 'Follows you';
                        break;
                    }
                }
                ?>
                <i><?= $follow ?></i>
                </small>
            </div>
        </div>
        <?php endforeach ?>
    </div>
    <!-- /#sidebar-wrapper -->
</div>
        <?= $this->Html->script('sidebars.js') ?>
    <?php endif ?>
<?php endif ?>
