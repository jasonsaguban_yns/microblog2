<?php if ($this->Session->read('Auth.User.id')) : ?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary py-0 px-0">
  <div class="container-fluid">
    <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'index']) ?>">
    <div class='text-white'>      
      <?= $this->Html->image("microblog-logo-white.png", [
              "alt" => "microBlog-logo",
              "style" => "width:50px;margin-left:30px;"
          ]);
        ?>
      <strong style='margin-right:50px;font-family:courier,arial,helvetica;'>microBLOG</strong>      
    </div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
      <input type="hidden" id="csrfToken" value=<?= $this->request->getParam('_csrfToken') ?>>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <form action=<?= $this->Url->build(['controller' => 'users','action' => 'searchuser']) ?> class="d-flex mx-auto col-5" type='GET'>
        <input name='val' class="form-control my-2" id="searchInput" type="search" placeholder="Search">
        <button type='submit' class="btn btn-primary btn-sm">
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-search" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
        <path fill-rule="evenodd" d="M10.442 10.442a1 1 0 0 1 1.415 0l3.85 3.85a1 1 0 0 1-1.414 1.415l-3.85-3.85a1 1 0 0 1 0-1.415z"/>
        <path fill-rule="evenodd" d="M6.5 12a5.5 5.5 0 1 0 0-11 5.5 5.5 0 0 0 0 11zM13 6.5a6.5 6.5 0 1 1-13 0 6.5 6.5 0 0 1 13 0z"/>
        </svg>
        </button>
      </form>
      <ul class="navbar-nav">
        <li class="nav-item active">
            <a href=<?= $this->Url->build(['controller' => 'posts','action'=>'index']) ?> class='btn btn-primary' >
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-house-door-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path d="M6.5 10.995V14.5a.5.5 0 0 1-.5.5H2a.5.5 0 0 1-.5-.5v-7a.5.5 0 0 1 .146-.354l6-6a.5.5 0 0 1 .708 0l6 6a.5.5 0 0 1 .146.354v7a.5.5 0 0 1-.5.5h-4a.5.5 0 0 1-.5-.5V11c0-.25-.25-.5-.5-.5H7c-.25 0-.5.25-.5.495z"/>
                <path fill-rule="evenodd" d="M13 2.5V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"/>
                </svg>
                Home
            </a>
        </li>
        <li class="nav-item">
          <a href=<?= $this->Url->build(['controller' => 'users','action'=>'view',$this->Session->read('Auth.User.username')]) ?> class='btn btn-primary' >
          <?= $this->Html->image(($this->Session->read('Auth.User.image_location')) ? h($this->Session->read('Auth.User.image_location')) : 'microblogicon.png', ['class' => 'float-left mr-1 mini-picture']) ?>
          <?= ucfirst(h($this->Session->read('Auth.User.first_name'))) ?>
          </a>
        </li>        
        <li class="nav-item ">
            <div class="btn-group">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                </button>
                <ul class="dropdown-menu dropdown-menu-right mt-2">
                    <li>
                        <?= $this->Html->link(
                            'Edit Profile',
                            ['controller' => 'users','action' => 'edit'],
                            ['class' => 'dropdown-item']
                        ) ?>                           
                    </li>
                    <li>
                        <?= $this->Html->link(
                            'Logout',
                            ['controller' => 'users','action' => 'logout'],
                            ['class' => 'dropdown-item text-danger']
                        ) ?>                           
                    </li>
                </ul>
            </div>
        </li>
      </ul>
    </div>
  </div>
</nav>
<?php endif ?>
