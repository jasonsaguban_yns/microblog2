<div class="mx-auto" style="max-width: 35rem;">

<?= $this->Form->create(false, ['url' => ['controller' => 'Comments', 'action' => 'add']]) ?>
<fieldset>
    <?php
        echo $this->Form->hidden('post_id', ['value' => $post['id']]);
        echo $this->Form->textarea('comment', array(
            'maxLength' => '140',
            'class' => 'mt-5 col-12',
            'rows' => '4',
            'required' => false,
            'id' => 'add-comment',
            'placeholder' => "What's your comment on this, ".ucfirst(h($this->Session->read('Auth.User.first_name')))."?"));
        echo $this->Form->button('Comment', array(
            'type' => 'submit' ,
            'class' => 'btn mt-1 btn-primary float-right'));
        ?>
    <small id="charCounter"></small>
</fieldset>
<?= $this->Form->end() ?>
<br>   

</div>
