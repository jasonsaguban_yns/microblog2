<?php use Cake\I18n\Time; ?>
<?php if (count($comments)) : ?>
<div class="card border-secondary mx-auto mb-3" style="max-width: 35rem;">
    <?php foreach ($comments as $comment) : ?>
        <?php $commentId = $comment['id'] ?>
    <div class="card-header" style="border-width: 1px;" id= <?= "commentDiv-".$commentId ?>>
        <!-- actions to the comment -->
        <div>
            <button class="btn btn-sm btn-danger float-right" style="display: none;" id=<?= "cancelBtn-".$commentId?>>Cancel</button>
            <div class="commentActions">
                <?php if ($comment['user_id'] == $this->Session->read('Auth.User.id')) : ?>
                    <div class="btn-group dropright float-right">
                        <!-- Delete comment -->
                        <button class="btn btn-outline-danger delete-comment actions" id=<?= $commentId ?> data-toggle="tooltip" title="Delete comment" data-original-title="Delete comment">
                        <svg width="1em" height="2em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $commentId ?>>
                        <path id=<?= $commentId ?> d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                        <path id=<?= $commentId ?> fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                        </svg>
                        </button>
                        <!-- Edit comment -->
                        <button class="btn btn-outline-secondary edit-comment actions" id=<?= $commentId ?> data-toggle="tooltip" title="Edit comment" data-original-title="Edit comment">
                        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil-square edit-svg" fill="currentColor" xmlns="http://www.w3.org/2000/svg" id=<?= $commentId?>>
                        <path id=<?= $commentId ?> d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456l-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                        <path id=<?= $commentId ?> fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>                        
                        </button>
                    </div>
                <?php endif ?>
            </div>
        </div>

        <!-- comment user informations -->
        <div>
            <a href=<?= $this->Url->build(['controller' => 'users','action' => 'view',$comment['user']['username']]) ?>>
            <?= $this->Html->image(($comment['user']['image_location']) ? h($comment['user']['image_location']) : 'microblogicon.png', ['class' => 'float-left mr-1 picture-2']) ?>
            
            <?= ucfirst(h($comment['user']['first_name']))." ".ucfirst(h($comment['user']['last_name'])) ?>
            <small class="card-subtitle text-muted" style="display:block">@<?= h($comment['user']['username']) ?></small>
            </a>
            <?php $postedDate = new Time($comment['modified']) ?>                            
            <p class="card-text" style="display:inline"><small class="text-muted">
            <?= $postedDate->timeAgoInWords(['format' => 'MMM , YYY','accuracy' => ['month' => 'month'],'end' => '1 year']) ?>
            </small></p>
            <?php if ($comment->user_id != $this->Session->read('Auth.User.id')) : ?>
                <?php
                $followBtnClass = "btn btn-outline-primary btn-sm float-right follow-button";
                $followBtnVal = "Follow";
                foreach ($followingList as $followingId) {
                    if ($followingId->following_user_id == $comment->user_id) {
                        $followBtnClass = "btn btn-primary btn-sm float-right follow-button followed";
                        $followBtnVal = "Following";
                        break;
                    }
                }
                ?>
            <button class="<?= $followBtnClass ?>"" style="display:inline" id=<?= $post->user_id ?>><?= $followBtnVal ?></button>
            <br>
            <?php endif ?>
            <hr class="bg-secondary">
        </div>

        <!-- contents of the comment section -->
        <p class= <?= 'card-text'.$commentId ?>><?= h($comment['comment']) ?></p>
    </div>
        <?php if ($comment['user_id'] == $this->Session->read('Auth.User.id')) : ?>
    <!-- editting of comment section -->
    <div style="display: none;" class = <?= "edit-comment".$commentId ?>>
        <textarea name="editComment" id=<?= "textarea-".$commentId ?> rows="5" maxLength ='140' class="col-12"  required='required'></textarea>
        <input type="hidden" name="commentId" value=<?= $commentId ?>>
        <button type="submit" class="btn btn-primary float-right" id=<?= "button-".$commentId ?>>Save Changes</button>
        <small class="ml-2" id=<?= "charCounter-".$commentId ?>></small>
    </div>
        <?php endif ?>
    <?php endforeach ?>
</div>
<?php endif ?>
