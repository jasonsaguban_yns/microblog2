<div class="container">
    <?= $this->element('addpost') ?>
</div>
<?php if (count($posts)) : ?>
    <?= $this->element('posts') ?>
<?php else : ?>
<p class="lead text-center text-primary mx-auto" style="max-width: 35rem;">
    You don't have any post yet. Post now or follow other users to see their post.
</p>
<?php endif ?>
<?= $this->element('pagination') ?>
