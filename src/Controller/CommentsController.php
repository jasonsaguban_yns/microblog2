<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class CommentsController extends AppController
{

    public function add()
    {
        $this->autoRender = false;
        $this->request->allowMethod('post');
        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());
            $comment['comment'] = substr(trim($comment['comment']), 0, 140);
            $postId = $comment['post_id'];
            $comment['user_id'] = $this->Auth->User('id');
            if ($comment['comment']) {
                if ($this->Comments->save($comment)) {
                    $this->Flash->success(__('The comment has been saved.'));
                    return $this->redirect(['controller'=>'posts','action' => 'view',$postId]);
                }
            } else {
                $this->Flash->error(__('Comment cannot be empty text'));
                return $this->redirect(['controller'=>'posts','action' => 'view',$postId]);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
            return $this->redirect(['controller'=>'posts','action' => 'view',$postId]);
        }
    }

    public function edit($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod('post');
        if ($this->request->is(['post'])) {
            if ($this->ownerChecker($id)) {
                $comment = $this->Comments->get($id);
                $comment = $this->Comments->patchEntity($comment, $this->request->getData());
                $comment['comment'] = substr(trim($comment['comment']), 0, 140);
                $comment['user_id'] = $this->Auth->user('id');
                if ($comment['comment']) {
                    if ($this->Comments->save($comment)) {
                        echo json_encode(array('status' => 'edited'));
                        exit;
                    }
                } else {
                    echo json_encode(array('status' => 'null'));
                    exit;
                }
            }
        }
    }

    public function delete($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $comment = $this->Comments->get($id);
        if ($this->ownerChecker($id)) {
            if ($this->Comments->delete($comment)) {
                echo json_encode(array('status' => 'deleted'));
            }
        }
    }

    public function ownerChecker($id = null)
    {
        $userId = $this->Auth->user('id');
        $comments = TableRegistry::get('Comments');
        $comment = $comments
            ->find()
            ->where(['id' => $id,'user_id' => $userId])
            ->first();
        if (empty($comment->comment)) {
            return false;
        } else {
            return true;
        }
    }
}
