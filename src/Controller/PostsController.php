<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class PostsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
        $userId = $this->Auth->user('id');
        $this->paginate = ['limit' => 10];
        $conditions = ['Posts.deleted IS NULL','Posts.user_id IN (SELECT following_user_id from followers WHERE follower_user_id = '.$userId.') OR Posts.user_id = '.$userId];
        $joins = $this->Common->postJoins();
        $fields = $this->Common->postFields();
        $posts = $this->paginate($this->Posts->findPosts($joins, $fields, $conditions));
        $this->set(compact('posts'));
    }

    public function view($id = null)
    {
        if ($id) {
            $post = $this->Posts->findPost($id, $this->Common->postJoins(), $this->Common->postFields());
            if (!$post) {
                $this->Flash->error(__('Post is invalid or has been deleted.'));
                return $this->redirect(['action'=>'index']);
            }
            $this->paginate = ['limit' => 10];
            $comment = TableRegistry::get('Comments');
            $comments = $this->paginate($comment->findComments($id));
            $this->set(compact('post', 'comments'));
        } else {
            return $this->redirect($this->referer());
            $this->Flash->error(__('Post is invalid or has been deleted.'));
        }
    }

    public function add()
    {
        $this->autoRender = false;
        $this->request->allowMethod('post');
        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post['post'] = substr(trim($post['post']), 0, 140);            
            $post['user_id'] = $this->Auth->User('id');            
            if (!$post->getErrors()) {
                if ($post['post'] || !empty($this->request->data['post_image']['type'])) {                
                    //Adding picture to the post
                    if (!empty($this->request->data['post_image']['type'])) {
                        $image = $this->request->getData('post_image');
                        $tmp = $image['tmp_name'];
                        $hash = rand();
                        $date = date("Ymd");
                        $image = $date.$hash;
                        $image_location = "post-images/".$image;
                        $targetPath = WWW_ROOT.'img'.DS.'post-images'.DS;
                        $targetPath = $targetPath.basename($image);
                        move_uploaded_file($tmp, $targetPath);
                        $post->post_image = $image_location;
                    } else {
                        $post->post_image = null;
                    }                    
                    if ($this->Posts->save($post)) {
                        $this->Flash->success(__('The post has been saved.'));
                        return $this->redirect($this->referer());
                    }
                } else {
                    $this->Flash->error(__('Post cannot be empty'));
                    return $this->redirect($this->referer());
                }
            } else {
                if (isset($post->getErrors()['post_image']['mimeType'])) {
                    $this->Flash->error(__($post->getErrors()['post_image']['mimeType']));
                }
                if (isset($post->getErrors()['post_image']['fileSize'])) {
                    $this->Flash->error(__($post->getErrors()['post_image']['fileSize']));
                }
                $this->request->session()->write('current-post', $post['post']);
                return $this->redirect($this->referer());
            }
        }
    }

    public function edit($id = null)
    {
        $this->autoRender = false;
        $post = $this->Posts->get($id);
        if ($this->request->is(['post'])) {
            if ($this->ownerChecker($id)) {
                $post = $this->Posts->patchEntity($post, $this->request->getData());
                $post['user_id'] = $this->Auth->user('id');
                $post['post'] = substr(trim($_POST['post']), 0, 140);                
                if (!$post->repost_id && empty($post['post'])) {
                    echo json_encode(array('status' => 'null'));
                    exit;
                }
                if ($this->Posts->save($post)) {
                    echo json_encode(array('status' => 'edited'));
                }
            }
        }
    }

    public function delete($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $post = $this->Posts->get($id);
        if ($this->ownerChecker($id)) {
            if ($post->repost_id) {
                $this->deleteRepost($post->repost_id);
            }
            if ($this->Posts->delete($post)) {
                echo json_encode(array('status' => 'deleted'));
                $this->Flash->success(__('Post deleted successfully!'));
            }
        }
    }

    public function ownerChecker($id = null)
    {
        $userId = $this->Auth->user('id');
        $post = $this->Posts
            ->find()
            ->where(['id' => $id,'user_id' => $userId])
            ->first();
        if (empty($post->id)) {
            return false;
        } else {
            return true;
        }
    }

    public function deleteRepost($id = null)
    {
        $this->loadModel('Reposts');
        $repost = $this->Reposts->get($id);
        $this->Reposts->delete($repost);
        return true;
    }
}
