<?php

namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Core\Configure;
use Cake\Event\Event;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\I18n\Time;

class AppController extends Controller
{

    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);
        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->loadComponent('Common');
        $this->loadComponent('Auth', [
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'username',
                        'password' => 'password'
                    ]
                ],
                'RememberMe.Cookie' => [
                    'userModel' => 'Users',
                    'fields' => ['username' => 'email'],
                    'inputKey' => 'remember_me',
                    'cookie' => [
                        'name' => 'rememberMe',
                        'expires' => '+30 days',
                        'secure' => false,
                        'always' => true,
                        'httpOnly' => true,
                    ]
                ]
            ],
            'loginRedirect' => [
                'controller' => 'Posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
             // If unauthorized, return them to page they were just on
            'unauthorizedRedirect' => $this->referer()
        ]);
        // Allow the display action so our PagesController
        // continues to work. Also enable the read only actions.
        // $this->Auth->allow(['display', 'view', 'index']);
    }

    public function isAuthorized($user)
    {
        // By default deny access.

        return false;
    }

    public function beforeRender(Event $event)
    {
        parent::beforeRender($event);
        try {
            if (!empty($this->Auth->user('id'))) {
                $userId = $this->Auth->user('id');
                if ($userId) {
                    $Users = TableRegistry::get('Users');
                    $suggest = $Users
                        ->find()
                        ->where(
                            [
                            'id NOT IN (SELECT following_user_id from followers WHERE follower_user_id = '.$userId.')','id != '.$userId,
                            'status != 0'
                            ]
                        )
                        ->order('rand()')
                        ->limit('10')
                        ->all();
                    $followerListId = $this->Common->getUserFollowerListId();
                    $followingList = $this->Common->getUserFollowingListId();
                    $this->set(compact('suggest', 'followerListId', 'followingList'));
                }
            }
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
