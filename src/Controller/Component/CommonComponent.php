<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class CommonComponent extends Component
{
    public $components = ['Auth'];
    
    public function postJoins()
    {
        $joins =  [
            [
                'table' => 'users',
                'alias' => 'user',
                'type' => 'LEFT',
                'conditions' => ['user.id = Posts.user_id']                                                
            ],
            [
                'table' => 'reposts',
                'alias' => 'Repost',
                'type' => 'LEFT',
                'conditions' => ['Repost.id = Posts.repost_id']                                                
            ],
            [
                'table' => 'posts',
                'alias' => 'Post_2',
                'type' => 'LEFT',
                'conditions' => ['Post_2.id = Repost.post_id']                                            
            ],
            [
                'table' => 'users',
                'alias' => 'User_2',
                'type' => 'LEFT',
                'conditions' => ['User_2.id = Post_2.user_id']                                            
            ]
        ];
        return $joins;
    }

    public function postFields()
    {
        $fields = [
            "Posts.id",
            "Posts.user_id",
            "Posts.post",
            "Posts.post_image",
            "Posts.repost_id",
            "Posts.created",
            "Posts.modified",
            'Posts.deleted',
            "user.id",
            "user.first_name",
            "user.last_name",
            "user.username",
            "user.image_location",
            "Repost.id",
            "Repost.post_id",
            "Post_2.id",
            "Post_2.user_id",
            "Post_2.post",
            "Post_2.post_image",
            "Post_2.created",
            "Post_2.deleted",
            "User_2.id",
            "User_2.first_name",
            "User_2.last_name",
            "User_2.username",
            "User_2.image_location"
        ];
        return $fields;
    }

    public function getUserFollowingListId()
    {
        $followers = TableRegistry::get('Followers');
        $followingList = $followers
            ->find()
            ->where(['follower_user_id' => $this->Auth->user('id')])
            ->select(['following_user_id'])
            ->all();
        return $followingList;
    }

    public function getUserFollowerListId()
    {
        $followers = TableRegistry::get('Followers');
        $followerList = $followers
            ->find()
            ->where(['following_user_id' => $this->Auth->user('id')])
            ->select(['follower_user_id'])
            ->all();
        return $followerList;
    }
}