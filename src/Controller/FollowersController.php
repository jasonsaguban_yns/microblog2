<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class FollowersController extends AppController
{

    public function add($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $userId = $this->Auth->user('id');
        $follower = $this->Followers->newEntity();
        if ($id && $id !== $userId) {
            if ($this->request->is('get')) {
                $follower = $this->Followers->patchEntity($follower, $this->request->getData());
                $follower->follower_user_id = $userId;
                $follower->following_user_id = $id;
                if ($this->ownerChecker($id)) {
                    echo json_encode(array('status' => 'followed'));
                    exit;
                }

                if ($this->Followers->save($follower)) {
                    echo json_encode(array('status' => 'followed'));
                    exit;
                }
            }
        }
    }

    public function delete($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $follower = $this->Followers
            ->find()
            ->where(['following_user_id' => $id,'follower_user_id' => $this->Auth->user('id')])
            ->first();
        if ($this->Followers->delete($follower)) {
            echo json_encode(array('status' => 'unfollowed'));
            exit;
        }
    }

    public function ownerChecker($id = null)
    {
        $userId = $this->Auth->user('id');
        $follower = $this->Followers
            ->find()
            ->where(['following_user_id' => $id,'follower_user_id' => $userId])
            ->first();
        if (empty($follower->id)) {
            return false;
        } else {
            return true;
        }
    }
}
