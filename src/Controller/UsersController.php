<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Network\Session\DatabaseSession;
use Cake\Mailer\Email;
use Cake\Routing\Router;
use Cake\Validation\Validation;

class UsersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['logout','register','verify','forgotpassword','changepassword']);
    }

    public function view($username = null)
    {        
        $user = $this->Users->findUser(['username' => $username,'status != 0']);
        $userId = $this->Auth->user('id');
        if (empty($user->id)) {
            $this->Flash->error(__('User is invalid or not found.'));
            return $this->redirect($this->referer());
        }
        $this->loadModel('Posts');
        $this->paginate = ['limit' => 10];
        
        $joins = $this->Common->postJoins();
        $fields = $this->Common->postFields();
        $conditions = ['Posts.user_id = '.$user->id];
        
        $posts = $this->paginate($this->Posts->findPosts($joins, $fields, $conditions));
        $this->loadModel('Followers');
        $followers = $this->Followers->findUserFollowers($user->id);
        $followings = $this->Followers->findUserFollowings($user->id);
        $this->set(compact('posts', 'user', 'followers', 'followings'));
    }

    public function register()
    {
        $this->loginChecker();
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user['status'] = 0;
            $token= bin2hex(random_bytes(16));
            $user['token'] = $token;
            if ($save = $this->Users->save($user)) {
                $data = [
                    'id' => $save->id,
                    'method' => 'verify',
                    'email' => $this->request->data['email'],
                    'message' => 'Click on the link below to complete your registration:',
                    'subject' => 'Complete Registration for MicroBlog',
                    'token' => $token
                ];
                if ($this->sendEmail($data)) {
                    $this->Flash->success(__('An activation link has been sent to your email, kindly check your inbox or spams.'));
                    return $this->redirect(['action' => 'login']);
                }
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    public function edit()
    {
        $id = $this->Auth->user('id');
        $user = $this->Users->get($id);
        $userCurrentImage = $user->image_location;
        $userCurrentQuote = $user->quote;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity(
                $user,
                $this->request->getData(),
                ['accessibleFields' => ['id' => false]]
            );

            //Changing profile picture
            if (!$user->getErrors()) {
                if (!empty($this->request->data['image_location']['name'])) {
                    $image = $this->request->getData('image_location');
                    $tmp = $image['tmp_name'];
                    $hash = rand();
                    $date = date("Ymd");
                    $image = $date.$hash;
                    $image_location = "uploads/".$image;
                    $targetPath = WWW_ROOT.'img'.DS.'uploads'.DS;
                    $targetPath = $targetPath.basename($image);
                    move_uploaded_file($tmp, $targetPath);
                    $user->image_location = $image_location;
                } else {
                    $user->image_location = $userCurrentImage;
                }
            }
            //Changing password
            if (isset($this->request->data['old_password'])) {
                $user = $this->Users->patchEntity(
                    $user,
                    [
                    'old_password'  => $this->request->data['old_password'],
                    'password'      => $this->request->data['password1'],
                    'password1'     => $this->request->data['password1'],
                    'password2'     => $this->request->data['password2']
                    ],
                    ['accessibleFields' => ['id' => false]]
                );
            }
            //Changing user quote
            if (isset($this->request->data['quote'])) {
                $user->quote = substr(trim($this->request->data['quote']), 0, 140);
            } else {
                $user->quote = $userCurrentQuote;
            }
            $user->id = $this->Auth->user('id');
            // print'<pre>'; print_r($user); exit;
            if ($this->Users->save($user)) {
                if ($this->Auth->user('id') === $user->id) {
                    $data = $user->toArray();
                    $this->Auth->setUser($data);
                }
                $this->Flash->success(__('Account has been edited succesfully.'));
                return $this->redirect(['action' => 'edit']);
            }
            $this->Flash->error(__('There was an error in editing your profile.'));
        }
        $this->set(compact('user'));
    }

    public function login()
    {
        $this->loginChecker();
        if ($this->request->is('post')) {
            if (Validation::email($this->request->data['username'])) {
                $this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ]);
                $this->Auth->constructAuthenticate();
                $this->request->data['email'] = $this->request->data['username'];
                unset($this->request->data['username']);
            }
            $user = $this->Auth->identify();
            if ($user) {
                if ($user['status'] !== 0) {
                    $this->Auth->setUser($user);
                    $this->Flash->success('Welcome to microBlog, '.ucfirst($user['first_name']));
                    return $this->redirect($this->Auth->redirectUrl(['controller' => 'posts', 'action' => 'index']));
                } else {
                    //resend activation email
                    $data = [
                        'id' => $user['id'],
                        'method' => 'verify',
                        'email' => $user['email'],
                        'message' => 'Click on the link below to complete your registration:',
                        'subject' => 'Complete Registration for MicroBlog',
                        'token' => $user['token']
                    ];
                    $this->sendEmail($data);
                    $this->Flash->error("Your account is inactive, we've resend an email to activate your account");
                }
            } else {
                $this->Flash->error('Your username or password is incorrect.');
            }
        }
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out, '.ucfirst($this->Auth->user('first_name')));
        return $this->redirect($this->Auth->logout());
    }

    public function verify($token = null, $id = null)
    {
        $this->loginChecker();
        $this->autoRender = false;
        $result = $this->Users->get($id);
        if (empty($result->id)) {
            $this->Flash->error("Account is invalid, kindly create another one.");
            $this->redirect($this->Auth->redirectUrl(['action' => 'register']));
        }
        if ($result->status == 0) {
            if ($result->token == $token) {
                $result->status = 1;
                if ($this->Users->save($result)) {
                    $this->Flash->success(__('Your account has been successfully activated.'));
                    $this->redirect($this->Auth->redirectUrl(['action' => 'login']));
                } else {
                    $this->Flash->error("Your registration failed please try again");
                    $this->redirect($this->Auth->redirectUrl(['action' => 'register']));
                }
            } else {
                $this->Flash->error("You have a wrong token.");
                $this->redirect($this->Auth->redirectUrl(['action' => 'register']));
            }
        } else {
            $this->Flash->success(__('Your account is already activated.'));
            $this->redirect($this->Auth->redirectUrl(['action' => 'login']));
        }
    }

    public function forgotpassword()
    {
        $this->loginChecker();
        if ($this->request->is('post')) {
            $user = $this->Users->findUser(['email' => $this->request->data['email']]);
            if ($user) {
                $token = bin2hex(random_bytes(16));
                $user->token = $token;
                if ($this->Users->save($user)) {
                    $data = [
                        'id' => $user->id,
                        'method' => 'change-password',
                        'email' => $this->request->data['email'],
                        'message' => 'It appears that you have forgotten your password. Kindly click the link below to change your password. If you did not request for a forgotten password, kindly ignore this email.',
                        'subject' => 'Forgotten password for microBlog',
                        'token' => $token
                    ];
                    if ($this->sendEmail($data)) {
                        $this->Flash->success(__('A link has been sent to your email to change your password.'));
                    }
                }
            } else {
                $this->Flash->error("This email is invalid or not found.");
            }
        }
    }

    public function changepassword($token = null, $id = null)
    {
        $this->loginChecker();
        $user = $this->Users->get($id);
        if ($user->token == $token) {
            if ($this->request->is(['patch', 'post', 'put'])) {
                $user = $this->Users->patchEntity(
                    $user,
                    [
                        'password'      => $this->request->data['password1'],
                        'password1'     => $this->request->data['password1'],
                        'password2'     => $this->request->data['password2']
                    ],
                    ['accessibleFields' => ['id' => false]]
                );
                if ($this->Users->save($user)) {
                    $this->Flash->success(__('Your password has been changed successfully.'));
                    return $this->redirect(['action' => 'login']);
                }
                $this->Flash->error(__('There was an error in changing your password.'));
            }
        } else {
            $this->Flash->error("Your link for forgotten password is invalid.");
            $this->redirect($this->Auth->redirectUrl(['action' => 'forgotpassword']));
        }
        $this->set(compact('user'));
    }

    public function sendEmail($data = null)
    {
        $userId = $data['id'];
        $method = $data['method'];
        $userEmail = $data['email'];
        $message = $data['message'];
        $subject = $data['subject'];
        $token = $data['token'];
        $baseUrl = Router::url('/', true);

        $email = new Email();
        $email->transport('gmail');
        $ms = $message;
        $ms.= $baseUrl.$method.'/'.$token.'/'.$userId;
        $ms = wordwrap($ms, 150);
        $email
            ->from('microblogbyjason.yns@gmail.com')
            ->to($userEmail)
            ->subject($subject)
            ->send($ms);
        return true;
    }

    public function loginChecker()
    {
        if ($this->Auth->user('id')) {
            $this->Flash->success(__('You are currently logged in'));
            return $this->redirect(['controller' => 'posts','action' => 'index']);
        }
    }

    public function searchuser()
    {
        $this->request->allowMethod('get');
        $searchVal = trim(strval($this->request->query['val']));
        if (!$searchVal) {
            $this->Flash->error("Search field cannot be empty");
            return $this->redirect($this->referer());
        }
        $this->paginate = [
            'limit' => 10,
            'conditions' => [
                "concat_ws(' ',first_name,last_name,username) LIKE '%".$searchVal."%'",
                'status != 0'
            ]
        ];
        $results = $this->paginate($this->Users);
        $post = TableRegistry::get('Posts');
        $postsCount = $post->searchPost($searchVal, $this->Common->postJoins());
        $resultsCount = $this->Users->searchUser($searchVal, $this->Auth->user('id'));
        $this->set(compact('results', 'searchVal', 'postsCount', 'resultsCount'));
    }

    public function searchpost()
    {
        $this->request->allowMethod('get');

        $searchVal = trim(strval($this->request->query['val']));
        $post = TableRegistry::get('Posts');
        $this->paginate = ['limit' => 10];
           
        $joins = $this->Common->postJoins();
        $fields = $this->Common->postFields();
        $conditions = ["Posts.deleted IS NULL","Posts.post LIKE '%".$searchVal."%'
        OR Post_2.post LIKE '%".$searchVal."%'"];

        $posts = $this->paginate($post->findPosts($joins, $fields, $conditions));
        $resultCount = $this->Users->searchUser($searchVal, $this->Auth->user('id'));
        $postsCount = $post->searchPost($searchVal, $this->Common->postJoins());
        
        $this->set(compact('searchVal', 'posts', 'resultCount', 'postsCount'));
    }
}
