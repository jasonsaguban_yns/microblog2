<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class LikesController extends AppController
{

    public function add($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $like = $this->Likes->newEntity();
        if ($this->request->is('get')) {
            if ($id) {
                if ($this->ownerChecker($id)) {
                    echo json_encode(array('status' => 'liked'));
                    exit;
                }
                $like = $this->Likes->patchEntity($like, $this->request->getData());
                $like['post_id'] = $id;
                $like['user_id'] = $this->Auth->user('id');
                if ($this->Likes->save($like)) {
                    echo json_encode(array('status' => 'liked'));
                    exit;
                }
            }
        }
    }

    public function delete($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['get']);
        $like = $this->Likes
            ->find()
            ->where(['post_id' => $id,'user_id' => $this->Auth->user('id')])
            ->first();

        if ($this->Likes->delete($like)) {
            echo json_encode(array('status' => 'deleted'));
            exit;
        }
    }

    public function ownerChecker($id = null)
    {
        $userId = $this->Auth->user('id');
        $like = $this->Likes
            ->find()
            ->where(['post_id' => $id,'user_id' => $userId])
            ->first();
        if (empty($like->id)) {
            return false;
        } else {
            return true;
        }
    }
}
