<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

class RepostsController extends AppController
{

    public function add($id = null)
    {
        $this->autoRender = false;
        $this->request->allowMethod(['post']);

        if ($id) {
            $userId = $this->Auth->user('id');
            $repost = $this->Reposts->newEntity();
            if ($this->request->is('post')) {
                $repost = $this->Reposts->patchEntity($repost, $this->request->getData());
                $repost['post_id'] = $id;
                $repost['user_id'] = $userId;
                if ($save = $this->Reposts->save($repost)) {
                    $repostId = $save->id;
                    $this->loadModel('Posts');
                    $data = substr(trim($_POST['post']), 0, 140);
                    $postData = [
                        'post' => ($data) ? $data : '',
                        'user_id' => $userId,
                        'repost_id' => $repostId
                    ];
                    $post = $this->Posts->newEntity();
                    $post = $this->Posts->patchEntity($post, $postData);
                    if ($this->Posts->save($post)) {
                        echo json_encode(array('status' => 'reposted'));
                        $this->Flash->success(__('Repost Successfully!'));
                    }
                }
            }
        }
    }
}
