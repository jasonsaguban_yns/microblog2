<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class FollowersTable extends Table
{

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('followers');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('FollowerUsers', [
            'foreignKey' => 'follower_user_id',
        ]);
        $this->belongsTo('FollowingUsers', [
            'foreignKey' => 'following_user_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        // $rules->add($rules->existsIn(['follower_user_id'], 'Users.id'));
        // $rules->add($rules->existsIn(['following_user_id'], 'Users.id'));
        return $rules;
    }

    public function findUserFollowers($userId)
    {
        $followers = $this
            ->find()
            ->join([
                [
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => ['User.id = Followers.Follower_user_id']
                ]
            ])
            ->where(['Followers.following_user_id' => $userId])
            ->select([
                'User.id',
                'User.first_name',
                'User.last_name',
                'User.username',
                'User.image_location'
            ])
            ->all();
        return $followers;
    }

    public function findUserFollowings($userId)
    {
        $followings = $this
            ->find()
            ->join([
                [
                    'table' => 'users',
                    'alias' => 'User',
                    'type' => 'LEFT',
                    'conditions' => ['User.id = Followers.Following_user_id']
                ]
            ])
            ->where(['Followers.follower_user_id' => $userId])
            ->select([
                'User.id',
                'User.first_name',
                'User.last_name',
                'User.username',
                'User.image_location'
            ])
            ->all();
        return $followings;
    }
}
