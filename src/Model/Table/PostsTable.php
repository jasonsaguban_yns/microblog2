<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

class PostsTable extends Table
{
    use SoftDeleteTrait;
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('posts');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);
        $this->belongsTo('Reposts', [
            'foreignKey' => 'repost_id',
        ]);
        $this->hasMany('Comments', [
            'foreignKey' => 'post_id',
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'post_id',
        ]);
        $this->hasMany('Reposts', [
            'foreignKey' => 'post_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->maxLength('post', 140)
            ->allowEmptyString('post');
            
        $validator
            ->allowEmpty('post_image')
            ->add(
                'post_image',
                [
                    'mimeType' => [
                        'rule' => array('mimeType', array( 'image/png', 'image/jpg', 'image/jpeg')),
                        'message' =>  'Please upload images only (png, jpg)'
                    ]
                ]
            );

        $validator
            ->add(
                'post_image',
                [
                    'fileSize' => [
                        'rule' => array('fileSize', '<=', '10MB'),
                        'message' => 'Image must be less than 10MB'
                    ]
                ]
            );


        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['repost_id'], 'Reposts'));
        return $rules;
    }

    public function findPost($postId, $joins, $fields)
    {
        $post = $this
            ->find()
            ->where(['Posts.id' => $postId])
            ->contain(['Comments','Likes','Reposts'])
            ->join($joins)
            ->select($fields)
            ->first();
        return $post;
    }

    public function findPosts($joins, $fields, $conditions)
    {
        $posts = $this
            ->find()
            ->contain(['Comments','Likes','Reposts'])
            ->join($joins)
            ->select($fields)
            ->where($conditions)
            ->order(['Posts.created' => 'desc']);
        return $posts;
    }

    public function searchPost($searchVal, $joins)
    {
        $result = $this
            ->find()
            ->join($joins)
            ->where(["Posts.deleted IS NULL","Posts.post LIKE '%".$searchVal."%'
            OR Post_2.post LIKE '%".$searchVal."%'"])
            ->count();
        return $result;
    }
}
