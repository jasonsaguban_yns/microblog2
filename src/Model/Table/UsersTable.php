<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\ORM\Entity;
use Cake\Validation\Validator;
use Cake\Auth\DefaultPasswordHasher;
use SoftDelete\Model\Table\SoftDeleteTrait;
use Cake\Event\Event;
use Cake\ORM\Rule\IsUnique;
use ArrayObject;

class UsersTable extends Table
{
    use SoftDeleteTrait;

    
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->hasMany('Comments', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Likes', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Posts', [
            'foreignKey' => 'user_id',
        ]);
        $this->hasMany('Reposts', [
            'foreignKey' => 'user_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('username', [
                'alphaNumeric'=> array(
                    'rule' => 'alphaNumeric',
                    'message' => 'Username can only be letters and numbers.'
                ),
                'between' => array(
                    'rule' => array('lengthBetween', 3, 15),
                    'message' => 'Username must be from 3 to 15 characters only.',
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please fill this field',
                )])
            ->add('password', [
                'minLenght' => array(
                    'rule' => array('minLength', '8'),
                    'message' => 'Password must have minimum of 8 characters long.'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please fill this field',
                )])
            ->add('first_name', [
                'custom' => array(
                    'rule' => array('custom','/^[a-zA-Z\s]*$/'),
                    'message' => 'First name must be letters only'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please fill this field',
                )])
            ->add('last_name', [
                'custom' => array(
                    'rule' => array('custom','/^[a-zA-Z\s]*$/'),
                    'message' => 'Last name must be letters only'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please fill this field',
                )])
            ->add('email', [
                'valid' => array(
                    'rule' => 'email',
                    'message' => 'Input a valid email'
                ),
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Please fill this field',
                )]);

        $validator
            ->add(
                'old_password',
                'custom',
                [
                'rule'=>  function ($value, $context) {
                    $user = $this->get($context['data']['id']);
                    if ($user) {
                        if ((new DefaultPasswordHasher)->check($value, $user->password)) {
                            return true;
                        }
                    }
                    return false;
                },
                'message'=>'The old password does not match the current password!',
                ]
            )
            ->notEmpty('old_password');
   
        $validator
            ->add('password1', [
                'length' => [
                    'rule' => array('minLength', '8'),
                    'message' => 'The password have to be at least 8 characters!',
                ]
            ])
            ->add('password1', [
                'match'=>[
                    'rule'=> ['compareWith','password2'],
                    'message'=>'The passwords does not match!',
                ]
            ])
            ->notEmpty('password1');

        $validator
            ->add('password2', [
                'length' => [
                    'rule' => array('minLength', '8'),
                    'message' => 'The password have to be at least 8 characters!',
                ]
            ])
            ->add('password2', [
                'match'=>[
                    'rule'=> ['compareWith','password1'],
                    'message'=>'The passwords does not match!',
                ]
            ])
            ->notEmpty('password2');
        
        $validator
            ->allowEmptyFile('image_location')
            ->add(
                'image_location',
                [
                'mimeType' => [
                    'rule' => array('mimeType', array( 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' =>  'Please upload images only (png, jpg)'
                ]
                ]
            );

        $validator
            ->add(
                'image_location',
                [
                'fileSize' => [
                    'rule' => array('fileSize', '<=', '10MB'),
                    'message' => 'Image must be less than 10MB'
                ],
                ]
            );

        $validator
            ->scalar('quote')
            ->maxLength('quote', 140)
            ->allowEmptyString('quote');

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], 'This username is already taken.'));
        $rules->add($rules->isUnique(['email'], 'This email has already been used.'));
        return $rules;
    }

    public function beforeMarshal(Event $event, ArrayObject $data)
    {
        if (isset($data['first_name'])) {
            $data['first_name'] = trim($data['first_name']);
        }
        if (isset($data['last_name'])) {
            $data['last_name'] = trim($data['last_name']);
        }
        if (isset($data['email'])) {
            $data['email'] = trim($data['email']);
        }
    }

    public function findUser($conditions)
    {
        $user = $this
            ->find()
            ->where($conditions)
            ->first();
        return $user;
    }

    public function searchUser($searchVal, $userId)
    {
        $result = $this
            ->find()
            ->where([
                "concat_ws(' ',first_name,last_name,username) LIKE '%".$searchVal."%'",
                'status != 0'
                ])
            ->count();
        return $result;
    }
}
