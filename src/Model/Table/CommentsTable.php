<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use SoftDelete\Model\Table\SoftDeleteTrait;

class CommentsTable extends Table
{
    use SoftDeleteTrait;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
        ]);
        $this->belongsTo('Posts', [
            'foreignKey' => 'post_id',
        ]);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->requirePresence('comment', true)
            ->notEmptyString('comment', 'Please fill this field')
            ->maxLength('comment', 140);

        $validation = [
            'comment' => array(
                'notEmpty' => array(
                    'rule' => 'notEmpty',
                    'message' => 'Comment cannot be empty text!'
                )
            )
        ];
            

        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['post_id'], 'Posts'));

        return $rules;
    }

    public function findComments($postId)
    {
        $comments = $this
            ->find()
            ->contain(['Users'])
            ->order(['Comments.created' => 'asc'])
            ->where(['Comments.post_id' => $postId]);
        return $comments;
    }
}
