<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Comment extends Entity
{
    
    protected $_accessible = [
        'user_id' => true,
        'post_id' => true,
        'comment' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'user' => true,
        'post' => true,
    ];
}
