<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Follower extends Entity
{

    protected $_accessible = [
        'follower_user_id' => true,
        'following_user_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'follower_user' => true,
        'following_user' => true,
    ];
}
