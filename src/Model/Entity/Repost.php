<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;


class Repost extends Entity
{

    protected $_accessible = [
        'post_id' => true,
        'user_id' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'posts' => true,
        'user' => true,
    ];
}
