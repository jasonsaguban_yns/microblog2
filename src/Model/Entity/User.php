<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

class User extends Entity
{

    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'username' => true,
        'email' => true,
        'password' => true,
        'image_location' => true,
        'status' => true,
        'token' => true,
        'created' => true,
        'modified' => true,
        'deleted' => true,
        'comments' => true,
        'likes' => true,
        'posts' => true,
        'reposts' => true,
    ];


    protected $_hidden = [
        'password'
    ];

    protected function _setPassword($value)
    {
        if (strlen($value)) {
            $hasher = new DefaultPasswordHasher();
            return $hasher->hash($value);
        }
    }
}
